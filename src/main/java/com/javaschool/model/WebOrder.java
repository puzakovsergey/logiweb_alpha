package com.javaschool.model;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.Set;

public class WebOrder {

    private Long id;

    private WebTruck truck;

    private WebDriver driver;

    private int timeForDelivery;

    private int maxWeight;

    private Date startDate;

    private Date endDate;

    private Set<WebGood> goods;

    public WebOrder() {
    }

    public Set<WebGood> getGoods() {
        return goods;
    }

    public void setGoods(Set<WebGood> goods) {
        this.goods = goods;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getTimeForDelivery() {
        return timeForDelivery;
    }

    public void setTimeForDelivery(int timeForDelivery) {
        this.timeForDelivery = timeForDelivery;
    }

    public int getMaxWeight() {
        return maxWeight;
    }

    public void setMaxWeight(int maxWeight) {
        this.maxWeight = maxWeight;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public WebTruck getTruck() {
        return truck;
    }

    public void setTruck(WebTruck truck) {
        this.truck = truck;
    }

    public WebDriver getDriver() {
        return driver;
    }

    public void setDriver(WebDriver driver) {
        this.driver = driver;
    }
}
