package com.javaschool.service;

import com.javaschool.entity.TruckEntity;
import com.javaschool.entity.driver.DriverEntity;
import com.javaschool.entity.geo.City;
import com.javaschool.entity.order.OrderEntity;
import com.javaschool.entity.order.WaypointsEntity;
import com.javaschool.entity.order.goods.GoodsEntity;
import com.javaschool.model.*;
import org.springframework.dao.DataAccessException;

import java.util.Date;
import java.util.List;
import java.util.Set;

public interface OrderService {
    Set<WebOrder> findAll() throws DataAccessException;
    OrderEntity findOrderById(Long id) throws DataAccessException;
    WebOrder findById(Long id) throws DataAccessException;
    WebOrder newOrder() throws DataAccessException;
    void deleteById(Long id) throws DataAccessException;
    OrderEntity computeDeliveryTimeAndMaxWeight(OrderEntity order) throws DataAccessException;
    WebOrder setTruck(WebOrder order) throws DataAccessException, IllegalArgumentException;
    WebOrder setDriver(WebOrder order) throws DataAccessException, IllegalArgumentException;
    Set<OrderEntity> findCompleteOrdersForLastDaysForDriver (DriverEntity driver) throws DataAccessException;
    WebOrder setGoods(WebOrder order) throws DataAccessException, IllegalArgumentException;
    int getDriverHoursOnDutyInMonth(DriverEntity driver) throws DataAccessException;
    List<WebWaypoint> getWaypointsList(WebOrder order) throws DataAccessException, IllegalArgumentException;
    WebWaypoint setWaypointReached(WebWaypoint webWaypoint) throws DataAccessException, IllegalArgumentException;
    Set<WebDriver> getDriversSetForOrder(WebOrder webOrder) throws DataAccessException, IllegalArgumentException;
    Set<WebTruck> getTrucksForOrder(WebOrder order) throws IllegalArgumentException, DataAccessException;
    Set<WebOrder> findOrdersByTruck(WebTruck truck) throws DataAccessException, IllegalArgumentException;
    Set<WebOrder> findOrdersByDriver(WebDriver driver) throws DataAccessException, IllegalArgumentException;
}
