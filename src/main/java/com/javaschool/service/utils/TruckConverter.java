package com.javaschool.service.utils;

import com.javaschool.entity.TruckEntity;
import com.javaschool.entity.geo.City;
import com.javaschool.model.WebTruck;

import java.util.HashSet;
import java.util.Set;

public class TruckConverter {
    public static WebTruck truckToWebTruck(TruckEntity truck) {
        WebTruck webTruck = new WebTruck();
        if (truck == null) {
            return webTruck;
        }
        webTruck.setId(truck.getId());
        webTruck.setLicencePlate(truck.getLicencePlate());
        webTruck.setCapacity(truck.getCapacity());
        webTruck.setFixed(truck.getFixed());
        webTruck.setCurrentCity(truck.getCurrentCity());
        webTruck.setNotes(truck.getNotes());
        if (truck.getOrder() == null) {
            webTruck.setOrderId(0L);
        } else {
            webTruck.setOrderId(truck.getOrder().getId());
        }
        return webTruck;
    }

    public static Set<WebTruck> trucksToWebTrucks(Set<TruckEntity> trucks){
        Set<WebTruck> webTrucks = new HashSet<>();
        if (trucks == null) {
            return webTrucks;
        }
        for (TruckEntity t : trucks) {
            if (t != null) {
                webTrucks.add(truckToWebTruck(t));
            }
        }
        return webTrucks;
    }

    public static TruckEntity webTruckToTruck(WebTruck truck) throws IllegalArgumentException {
        if (truck == null || truck.getLicencePlate() == null) {
            throw new IllegalArgumentException("Bad parameters");
        }
        TruckEntity truckEntity = new TruckEntity();
        truckEntity.setLicencePlate(truck.getLicencePlate());
        truckEntity.setCapacity(truck.getCapacity());
        truckEntity.setCurrentCity(truck.getCurrentCity());
        truckEntity.setFixed(truck.isFixed());
        truckEntity.setNotes(truck.getNotes());
        if (truck.getCurrentCity() == null) {
            truckEntity.setCurrentCity(City.VORONEZH);
        } else {
            truckEntity.setCurrentCity(truck.getCurrentCity());
        }
        return truckEntity;
    }
}
