package com.javaschool.service.utils;

import com.javaschool.entity.order.OrderEntity;
import com.javaschool.entity.order.goods.GoodsEntity;
import com.javaschool.model.WebDriver;
import com.javaschool.model.WebGood;
import com.javaschool.model.WebOrder;
import com.javaschool.model.WebTruck;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class OrderConverter {
    public static WebOrder orderToWebOrder(OrderEntity order) {
        if (order == null) {
            return new WebOrder();
        } else {
            WebOrder webOrder = new WebOrder();
            webOrder.setId(order.getId());
            if (order.getTruck() == null) {
                WebTruck truck = new WebTruck();
                truck.setLicencePlate("");
                webOrder.setTruck(truck);
            } else {
                webOrder.setTruck(TruckConverter.truckToWebTruck(order.getTruck()));
            }
            if (order.getDriver() == null) {
                WebDriver driver = new WebDriver();
                driver.setFirstName("");
                driver.setLastName("");
                webOrder.setDriver(driver);
            } else {
                webOrder.setDriver(DriverConverter.driverToWebDriver(order.getDriver()));
            }
            if (order.getTimeForDelivery() == null) {
                webOrder.setTimeForDelivery(0);
            } else {
                webOrder.setTimeForDelivery(order.getTimeForDelivery());
            }
            if (order.getStartDate() == null) {
                webOrder.setStartDate(new Date(0));
            } else {
                webOrder.setStartDate(order.getStartDate());
            }

            if (order.getEndDate() == null) {
                webOrder.setEndDate(new Date(0));
            } else {
                webOrder.setEndDate(order.getEndDate());
            }

            Set<WebGood> goods = new HashSet<>();
            if (order.getGoods() == null) {
                webOrder.setGoods(goods);
            } else {
                for (GoodsEntity g : order.getGoods()) {
                    goods.add(GoodConverter.goodToWebGood(g));
                }
                webOrder.setGoods(goods);
            }
            return webOrder;
        }
    }

    public static Set<WebOrder> ordersToWebOrders(Set<OrderEntity> orders) {
        if (orders == null) {
            return new HashSet<>();
        }
        Set<WebOrder> webOrders = new HashSet<>();
        for (OrderEntity o : orders) {
            if (o != null) {
                webOrders.add(orderToWebOrder(o));
            }
        }
        return webOrders;
    }
}
