package com.javaschool.service;

import com.javaschool.dao.DriverCrudRepository;
import com.javaschool.entity.driver.DriverEntity;
import com.javaschool.entity.driver.DriverStatus;
import com.javaschool.entity.geo.City;
import com.javaschool.entity.order.OrderEntity;
import com.javaschool.entity.user.UserEntity;
import com.javaschool.model.WebDriver;
import com.javaschool.model.WebOrder;
import com.javaschool.model.WebUser;
import com.javaschool.service.utils.DriverConverter;
import com.javaschool.service.utils.Utils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
@PropertySource("classpath:logiweb.properties")
@EnableJpaRepositories(basePackages = {"com.javaschool.dao"})
public class DriverServiceImpl implements DriverService {
    private DriverCrudRepository driverCrudRepository;
    private UserService userService;

    private final static Logger LOGGER = Logger.getLogger(DriverServiceImpl.class);

    @Value("${application.maxHoursInMonth:170}")
    private int maxHoursInMonth;

    public DriverServiceImpl(DriverCrudRepository driverCrudRepository, UserService userService) {
        this.driverCrudRepository = driverCrudRepository;
        this.userService = userService;
    }

    @Override
    @Transactional
    public Set<WebDriver> findAll() throws DataAccessException {
        Set<DriverEntity> drivers = driverCrudRepository.findAll();
        LOGGER.info("All drivers found");
        return DriverConverter.driversToWebDrivers(drivers);
    }

    @Override
    @Transactional
    public Set<WebDriver> findByName(String name) throws DataAccessException {
        if (name == null || name.equals("")) {
            return new HashSet<>();
        }
        Set<DriverEntity> drivers = driverCrudRepository.findByName(name);
        return DriverConverter.driversToWebDrivers(drivers);
    }

    @Override
    @Transactional
    public Set<WebDriver> findByStatus(DriverStatus status) throws DataAccessException {
        Set<DriverEntity> drivers = driverCrudRepository.findAllByStatus(status);
        return DriverConverter.driversToWebDrivers(drivers);
    }


    @Override
    @Transactional
    public DriverEntity findById(Long id) throws DataAccessException {
        DriverEntity driver = driverCrudRepository.findById(id).orElse(new DriverEntity());
        if (driver.getId() == null) {
            throw new IllegalArgumentException("Not found");
        }
        return driver;
    }

    @Override
    @Transactional
    public WebDriver findByIdC(Long id) throws DataAccessException {
        WebDriver driver = DriverConverter.driverToWebDriver(driverCrudRepository.findById(id).orElse(new DriverEntity()));
        if (driver.getId() == null) {
            throw new IllegalArgumentException("Not found");
        }
        return driver;
    }

    @Override
    @Transactional
    public WebDriver newDriver(WebDriver driver) throws DataAccessException, IllegalArgumentException {
            DriverEntity driverEntity = DriverConverter.webDriverToDriver(driver);
            WebDriver webDriver = DriverConverter.driverToWebDriver(driverCrudRepository.save(driverEntity));
            return webDriver;
    }

    @Override
    @Transactional
    public void deleteById(Long id) throws DataAccessException, IllegalArgumentException {
        DriverEntity driver = findById(id);
        if (driver.getStatus() == DriverStatus.ON_REST) {
            driverCrudRepository.deleteById(id);
        } else {
            throw new IllegalArgumentException("Can't delete driver, status = " + driver.getStatus());
        }
    }

    @Override
    @Transactional
    public WebDriver update(WebDriver driver) throws DataAccessException, IllegalArgumentException {
        if (driver == null) {
            throw new IllegalArgumentException("Wrong parameters");
        }
        if (driver.getId() == null) {
            throw new IllegalArgumentException("Wrong parameters");
        }
            DriverEntity driverEntity = findById(driver.getId());
        if (driverEntity.getOrder() == null) {
            if (driver.getFirstName() != null) {
                driverEntity.setFirstName(driver.getFirstName());
            }
            if (driver.getLastName() != null) {
                driverEntity.setLastName(driver.getLastName());
            }
            if (driver.getBirthDate() != null) {
                driverEntity.setBirthDate(driver.getBirthDate());
            }
            if (driver.getStatus() != null) {
                driverEntity.setStatus(driver.getStatus());
            }
            if (driver.getCurrentCity() != null) {
                driverEntity.setCurrentCity(driver.getCurrentCity());
            }
            if (driver.getUser() != null) {
                if (driver.getUser().getId() != null) {
                    WebUser user = userService.update(driver.getUser());
                    driverEntity.setUser(userService.findById(user.getId()));
                } else {
                    WebUser user = userService.newUser(driver.getUser());
                    driverEntity.setUser(userService.findById(user.getId()));
                }
            }
            WebDriver webDriver = DriverConverter.driverToWebDriver(driverCrudRepository.save(driverEntity));

            return webDriver;
        } else {
            throw new IllegalArgumentException("Can't update driver. Driver is on duty");
        }

    }

    @Override
    @Transactional
    public Set<DriverEntity> getFreeDriversByCity (City city) {
        Set<DriverEntity> drivers = driverCrudRepository.findAllByStatusAndCurrentCity(DriverStatus.ON_REST, city);
        return drivers;
    }

    @Override
    @Transactional
    public DriverEntity saveDriver(DriverEntity driver) throws DataAccessException {
        return driverCrudRepository.save(driver);
    }

//    @Override
//    @Transactional
//    public DriverEntity updateCityById(Long id, City currentCity) throws DataAccessException, IllegalArgumentException {
//        if (currentCity != null) {
//            DriverEntity driver = findById(id);
//            driver.setCurrentCity(currentCity);
//            return driverCrudRepository.save(driver);
//        }
//        throw new IllegalArgumentException("Wrong parameters");
//    }
//
//    @Override
//    @Transactional
//    public DriverEntity updateStatusById(Long id, DriverStatus status) throws DataAccessException, IllegalArgumentException {
//        if (status != null) {
//            DriverEntity driver = findById(id);
//            driver.setStatus(status);
//            return driverCrudRepository.save(driver);
//        }
//        throw new IllegalArgumentException("Wrong parameters");
//    }
//
//    @Override
//    @Transactional
//    public DriverEntity updateUserById(Long id, UserEntity user) throws DataAccessException, IllegalArgumentException {
//        if (user != null) {
//            DriverEntity driver = findById(id);
//            driver.setUser(user);
//            return driverCrudRepository.save(driver);
//        }
//        throw new IllegalArgumentException("Wrong parameters");
//    }
//
//    @Override
//    @Transactional
//    public WebUser getUserById(Long id) throws Exception {
//        DriverEntity driver = findById(id);
//        UserEntity user = driver.getUser();
//        if (user != null) {
//            return Utils.userToWebUser(user);
//        }
//        throw new Exception("User not assigned");
//    }
//
//    @Override
//    @Transactional
//    public DriverEntity updateOrderById(Long id, OrderEntity order) throws DataAccessException, IllegalArgumentException {
//        if (order != null) {
//            DriverEntity driver = findById(id);
//            driver.setOrder(order);
//            return driverCrudRepository.save(driver);
//        }
//        throw new IllegalArgumentException("Wrong parameters");
//    }
//
//    @Override
//    @Transactional
//    public WebOrder getOrderById(Long id) throws DataAccessException{
//        DriverEntity driver = findById(id);
//        OrderEntity order = driver.getOrder();
//        return Utils.orderToWebOrder(order);
//    }

//    @Override
//    @Transactional
//    public Set<DriverEntity> findFreeDriversForOrder(OrderEntity order) throws DataAccessException {
//        if (order != null && order.getTruck() != null && order.getTimeForDelivery()!=0) {
//            Calendar date = new GregorianCalendar();
//            date.add(Calendar.DATE, -30);
//            Set<OrderEntity> orders = orderService.findOrdersForLastDays(date.getTime());
//            Map<DriverEntity, Integer> driversHoursMap = new HashMap<>();
//            for (OrderEntity o : orders) {
//                DriverEntity driver = o.getDriver();
//                City city = order.getTruck().getCurrentCity();
//                if (driver.getCurrentCity().equals(city) && driver.getStatus() == DriverStatus.ON_REST) {
//                    int hoursOnDuty = o.getEndDate().getHours()-o.getStartDate().getHours();
//                    driversHoursMap.merge(driver, 0, (oldVal, newVal) -> oldVal + newVal);
//                }
//            }
//            for (Map.Entry<DriverEntity, Integer> entry: driversHoursMap.entrySet()) {
//                Integer hours = entry.getValue();
//                if ((hours + order.getTimeForDelivery() > maxHoursInMonth)) {
//                    driversHoursMap.remove(entry.getKey());
//                }
//            }
//            return driversHoursMap.keySet();
//        }
//        return new HashSet<>();
//    }



}
