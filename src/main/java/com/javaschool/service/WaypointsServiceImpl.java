package com.javaschool.service;

import com.javaschool.dao.WaypointsCrudRepository;
import com.javaschool.entity.driver.DriverStatus;
import com.javaschool.entity.order.OrderEntity;
import com.javaschool.entity.order.WaypointsEntity;
import com.javaschool.entity.order.WaypointsType;
import com.javaschool.entity.order.goods.GoodsEntity;
import com.javaschool.model.WebWaypoint;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
@EnableJpaRepositories(basePackages = {"com.javaschool.dao"})
public class WaypointsServiceImpl implements WaypointsService {
    private WaypointsCrudRepository waypointsCrudRepository;
    private final static Logger LOGGER = Logger.getLogger(UserServiceImpl.class);

    public WaypointsServiceImpl(WaypointsCrudRepository waypointsCrudRepository) {
        this.waypointsCrudRepository = waypointsCrudRepository;
    }

    @Override
    @Transactional
    public void deleteById(Long id) throws DataAccessException {
        waypointsCrudRepository.deleteById(id);
    }

    @Override
    @Transactional
    public WaypointsEntity findById(Long id) throws DataAccessException {
        WaypointsEntity waypoint = waypointsCrudRepository.findById(id).orElse(new WaypointsEntity());
        if (waypoint == null) {
            throw new IllegalArgumentException("There is no waypoint with id = " + id);
        }
        return waypoint;
    }

    @Override
    @Transactional
    public void deleteByIds(Set<Long> ids) throws DataAccessException, IllegalArgumentException {
        if (ids == null) {
            throw new IllegalArgumentException("Wrong parameters.");
        }
        waypointsCrudRepository.deleteByIdIn(ids);
    }

    @Override
    @Transactional
    public Set<WaypointsEntity> saveAll(Set<WaypointsEntity> waypoints) throws DataAccessException {
        Set<WaypointsEntity> savedWaypoints = new HashSet<>();
        for (WaypointsEntity w : waypoints) {
            savedWaypoints.add(waypointsCrudRepository.save(w));
        }
        return savedWaypoints;
    }

    @Override
    @Transactional
    public WaypointsEntity saveWaypoint(WaypointsEntity waypoint) throws DataAccessException {
        return waypointsCrudRepository.save(waypoint);
    }

    @Override
    @Transactional
    public Set<WaypointsEntity> getSetOfWaypointsByOrderId(Long id) throws DataAccessException {
        return waypointsCrudRepository.findAllByOrder_Id(id);
    }

    @Override
    @Transactional
    public void newWaypoint(WaypointsEntity waypoint) throws DataAccessException {
        waypointsCrudRepository.save(waypoint);
    }
}
