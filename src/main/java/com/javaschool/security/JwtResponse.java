package com.javaschool.security;

import org.springframework.http.HttpStatus;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.Date;

public class JwtResponse {

    private final String token;
    private final Date expiresAt;
    private final Integer status;
    private final String role;

    public JwtResponse(String token, Date expiresAt, Integer status, String role) {
        this.token = token;
        this.expiresAt = expiresAt;
        this.status = status;
        this.role = role;
    }

    public String getToken() {
        return token;
    }

    public Date getExpiresAt() {
        return expiresAt;
    }

    public Integer getStatus() {
        return status;
    }

    public String getRole() {
        return role;
    }
}
