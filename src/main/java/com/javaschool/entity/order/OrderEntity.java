package com.javaschool.entity.order;

import com.javaschool.entity.driver.DriverEntity;
import com.javaschool.entity.TruckEntity;
import com.javaschool.entity.order.goods.GoodsEntity;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "Orders")
public class OrderEntity {
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "Truck_ID")
    private TruckEntity truck;

    @OneToOne( fetch = FetchType.LAZY)
    @JoinColumn(name = "Driver_ID")
    private DriverEntity driver;

    @Column(name = "Time_For_Delivery")
    private Integer timeForDelivery;

    @Column(name = "Max_Weight")
    private Integer maxWeight;

    @Column(name = "Start_Date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date startDate;

    @Column(name = "End_Date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;

    @OneToMany(mappedBy = "order", fetch = FetchType.LAZY)
    private Set<GoodsEntity> goods;

    @OneToMany(mappedBy = "order", fetch = FetchType.LAZY)
    private Set<WaypointsEntity> waypoints;

    @Column(name = "Notes")
    private String notes = "";

    public OrderEntity() {
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TruckEntity getTruck() {
        return truck;
    }

    public void setTruck(TruckEntity truck) {
        this.truck = truck;
    }

    public DriverEntity getDriver() {
        return driver;
    }

    public void setDriver(DriverEntity driver) {
        this.driver = driver;
    }

    public Integer getTimeForDelivery() {
        return timeForDelivery;
    }

    public void setTimeForDelivery(Integer timeForDelivery) {
        this.timeForDelivery = timeForDelivery;
    }

    public Integer getMaxWeight() {
        return maxWeight;
    }

    public Set<WaypointsEntity> getWaypoints() {
        return waypoints;
    }

    public void setWaypoints(Set<WaypointsEntity> waypoints) {
        this.waypoints = waypoints;
    }

    public void setMaxWeight(Integer maxWeight) {
        this.maxWeight = maxWeight;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Set<GoodsEntity> getGoods() {
        return goods;
    }

    public void setGoods(Set<GoodsEntity> goods) {
        this.goods = goods;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof OrderEntity)) return false;
        OrderEntity that = (OrderEntity) o;
        return id.equals(that.id) &&
                Objects.equals(truck, that.truck) &&
                Objects.equals(driver, that.driver) &&
                Objects.equals(timeForDelivery, that.timeForDelivery) &&
                Objects.equals(maxWeight, that.maxWeight) &&
                Objects.equals(startDate, that.startDate) &&
                Objects.equals(endDate, that.endDate) &&
                Objects.equals(goods, that.goods) &&
                Objects.equals(waypoints, that.waypoints) &&
                Objects.equals(notes, that.notes);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, truck, driver, timeForDelivery, maxWeight, startDate, endDate, goods, waypoints, notes);
    }
}
