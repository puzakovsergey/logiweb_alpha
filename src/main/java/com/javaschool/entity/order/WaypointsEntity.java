package com.javaschool.entity.order;

import com.javaschool.entity.geo.City;
import com.javaschool.entity.order.goods.GoodsEntity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "Waypoints")
public class WaypointsEntity {
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "Good_ID", nullable = false)
    private GoodsEntity good;

    @Column(name = "Number", nullable = false)
    private Integer number;

    @Column(name = "City", nullable = false)
    @Enumerated(EnumType.STRING)
    private City city;

    @Column(name = "Type", nullable = false)
    @Enumerated(EnumType.STRING)
    private WaypointsType type;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "Order_ID", nullable = false)
    private OrderEntity order;

    public WaypointsEntity() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public GoodsEntity getGood() {
        return good;
    }

    public void setGood(GoodsEntity good) {
        this.good = good;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public WaypointsType getType() {
        return type;
    }

    public void setType(WaypointsType type) {
        this.type = type;
    }

    public OrderEntity getOrder() {
        return order;
    }

    public void setOrder(OrderEntity order) {
        this.order = order;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof WaypointsEntity)) return false;
        WaypointsEntity that = (WaypointsEntity) o;
        return id.equals(that.id) &&
                good.equals(that.good) &&
                number.equals(that.number) &&
                city == that.city &&
                type == that.type &&
                order.equals(that.order);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, good, number, city, type, order);
    }
}
