package com.javaschool.entity.order.goods;

import com.javaschool.entity.geo.City;
import com.javaschool.entity.order.OrderEntity;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "Goods")
public class GoodsEntity {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "City_For_Loading", nullable = false)
    @Enumerated(EnumType.STRING)
    private City cityForLoading;

    @Column(name = "City_For_Unloading", nullable = false)
    @Enumerated(EnumType.STRING)
    private City cityForUnloading;

    @Column(name = "Name", nullable = false)
    private String name;

    @Column(name = "Date_Ready")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateReady;

    @Column(name = "Date_Loaded")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateLoaded;

    @Column(name = "Date_Unloaded")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateUnloaded;

    @Column(name = "Weight", nullable = false)
    private Integer weight;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "Order_ID")
    private OrderEntity order;

    @Column(name = "Notes")
    private String notes = "";

    public GoodsEntity() {
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public City getCityForLoading() {
        return cityForLoading;
    }

    public void setCityForLoading(City cityForLoading) {
        this.cityForLoading = cityForLoading;
    }

    public City getCityForUnloading() {
        return cityForUnloading;
    }

    public void setCityForUnloading(City cityForUnloading) {
        this.cityForUnloading = cityForUnloading;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDateReady() {
        return dateReady;
    }

    public void setDateReady(Date dateReady) {
        this.dateReady = dateReady;
    }

    public Date getDateLoaded() {
        return dateLoaded;
    }

    public void setDateLoaded(Date dateLoaded) {
        this.dateLoaded = dateLoaded;
    }

    public Date getDateUnloaded() {
        return dateUnloaded;
    }

    public void setDateUnloaded(Date dateUnloaded) {
        this.dateUnloaded = dateUnloaded;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    public OrderEntity getOrder() {
        return order;
    }

    public void setOrder(OrderEntity order) {
        this.order = order;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof GoodsEntity)) return false;
        GoodsEntity that = (GoodsEntity) o;
        return id.equals(that.id) &&
                cityForLoading == that.cityForLoading &&
                cityForUnloading == that.cityForUnloading &&
                name.equals(that.name) &&
                Objects.equals(dateReady, that.dateReady) &&
                Objects.equals(dateLoaded, that.dateLoaded) &&
                Objects.equals(dateUnloaded, that.dateUnloaded) &&
                weight.equals(that.weight) &&
                Objects.equals(order, that.order) &&
                Objects.equals(notes, that.notes);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, cityForLoading, cityForUnloading, name, dateReady, dateLoaded, dateUnloaded, weight, order, notes);
    }
}
