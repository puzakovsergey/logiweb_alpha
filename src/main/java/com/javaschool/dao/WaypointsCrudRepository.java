package com.javaschool.dao;

import com.javaschool.entity.order.OrderEntity;
import com.javaschool.entity.order.WaypointsEntity;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;
import java.util.Set;

@Repository
public interface WaypointsCrudRepository extends CrudRepository<WaypointsEntity, Long> {
    Set<WaypointsEntity> findAllByOrder(OrderEntity order);
    void deleteByIdIn (Collection<Long> ids);
    Set<WaypointsEntity> findAllByOrder_Id(Long id);
}
