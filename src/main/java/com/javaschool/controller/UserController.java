package com.javaschool.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.javaschool.entity.user.UserEntity;
import com.javaschool.model.WebUser;
import com.javaschool.service.OrderService;
import com.javaschool.service.UserService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.Set;

@CrossOrigin
@RestController
@RequestMapping(path = "/api/user", produces = MediaType.APPLICATION_JSON_VALUE)
public class UserController {
    private UserService userService;
    private OrderService orderService;
    private ObjectMapper objectMapper;
    private final static Logger LOGGER = Logger.getLogger(UserController.class);

    public UserController(UserService userService, OrderService orderService, ObjectMapper objectMapper) {
        this.userService = userService;
        this.orderService = orderService;
        this.objectMapper = objectMapper;
    }

    private boolean isValid(WebUser webUser) {
        return webUser != null && webUser.getLogin() != null && webUser.getPassword() != null && webUser.getRole() != null;
    }

    @PostMapping("/login")
    public ResponseEntity<String> userLogin(@RequestBody WebUser webUser) {
        LOGGER.info("Try to login with " + webUser.toString());
        if (isValid(webUser)) {
            try {
                WebUser returnWebUser = userService.loginProcedure(webUser.getLogin(), webUser.getPassword());
                return new ResponseEntity<>(objectMapper.writeValueAsString(returnWebUser), HttpStatus.OK);
            } catch (DataAccessException e) {
                LOGGER.error("Login or password invalid " + webUser.toString());
                return new ResponseEntity<>("Login or password is invalid", HttpStatus.NOT_FOUND);
            } catch (JsonProcessingException e) {
                LOGGER.error("Json mapper exception with " + e.getMessage());
                return new ResponseEntity<>("Json mapper exception", HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
        LOGGER.error("Bad request with " + webUser.toString());
        return new ResponseEntity<>("", HttpStatus.BAD_REQUEST);
    }

   @GetMapping("/all")
    public ResponseEntity<String> getAllUsers() {
       LOGGER.info("Try to get all users");
       try {
           Set<WebUser> users = userService.findAll();
           return new ResponseEntity<>(objectMapper.writeValueAsString(users), HttpStatus.OK);
       } catch (JsonProcessingException e) {
           LOGGER.error("Json mapper exception with " + e.getMessage());
           return new ResponseEntity<>("Json mapper exception", HttpStatus.INTERNAL_SERVER_ERROR);
       }
   }
    @GetMapping("/details")
    public ResponseEntity<String> userDetailsGet(@RequestParam int id) {
        LOGGER.info("Try to get user with id= " + id);
        try {
            WebUser user = userService.findByIdC((long)id);
            return new ResponseEntity<>(objectMapper.writeValueAsString(user), HttpStatus.OK);
        } catch (DataAccessException e) {
            LOGGER.error("User with id= " + id + " not found");
            return new ResponseEntity<>("", HttpStatus.NOT_FOUND);
        } catch (JsonProcessingException e) {
            LOGGER.error("Json mapper exception with " + e.getMessage());
            return new ResponseEntity<>("Json mapper exception", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @PutMapping("/update")
    public ResponseEntity<String> userUpdate(@RequestBody WebUser webUser){
        LOGGER.info("Try to update user with login= " + webUser.getLogin());
            try {
                webUser = userService.update(webUser);
                return new ResponseEntity<>(objectMapper.writeValueAsString(webUser), HttpStatus.OK);
            } catch (DataAccessException | JsonProcessingException e) {
                LOGGER.error("User with login= " + webUser.getLogin() + " not found to update");
                return new ResponseEntity<>("", HttpStatus.NOT_FOUND);
            }
    }

    @DeleteMapping("/delete")
    public ResponseEntity<String> userDelete(@RequestParam int id) {
        LOGGER.info("Try to delete user with id= " + id);
        try {
            userService.deleteById((long)id);
            return new ResponseEntity<>("deleted", HttpStatus.OK);
        } catch (DataAccessException e) {
            LOGGER.error("User with id= " + id + " not found to delete");
            return new ResponseEntity<>("", HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/new")
    public ResponseEntity<String> newUser(@RequestBody WebUser webUser){
        if (isValid(webUser)) {
            try {
                webUser = userService.newUser(webUser);
                return new ResponseEntity<>(objectMapper.writeValueAsString(webUser), HttpStatus.OK);
            } catch (DataAccessException e) {
                return new ResponseEntity<>("", HttpStatus.CONFLICT);
            } catch (JsonProcessingException e) {
                return new ResponseEntity<>("Json mapper exception", HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else {
            return new ResponseEntity<>("", HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping
    public ResponseEntity<String> login (@RequestBody String username, String password){
        try {
            if (username.equals("user") && password.equals("password")) {
                return new ResponseEntity<>("", HttpStatus.OK);
            } else {
                return new ResponseEntity<>("", HttpStatus.FORBIDDEN);
            }

        } catch (DataAccessException e) {
            return new ResponseEntity<>("", HttpStatus.FORBIDDEN);
        }
    }

}
