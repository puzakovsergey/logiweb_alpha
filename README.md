# Logiweb
This is the Logiweb application. This application developed for education purposes.
This application simulates business processes in a small delivery company 

## Getting Started

You'll have to start this application from IDE for now.

### Prerequisites

You need to install Tomcat 9.0.27 or higher for deployment.
DataBase is embedded. It is H2 Database.

## Running the tests

Tests run with Mavens "test" command.

## Built With

SpringMVC
SpringData
Liquibase

## Authors

* **Sergey Puzakov** 

## Development plan
1. Creation of technical specifications;
2. Technology selection:
   - Tomcat standalone server
   - Spring MVC;
   - Spring Data;
   - Spring Secure
   - H2 embedded database;
   - Liquibase;
   - Angular.
3. Creation of database structure;
4. Creation of REST API; {in progress}
---- {we are here for now} ----
5. Implementation of Spring Secure;
5. Creation of frontend application;
6. Creation user interface for online dashboard;
7. Deployment on cloud service.

## License

This project is unlicensed.
